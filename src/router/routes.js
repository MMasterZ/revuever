const routes = [{
    path: "/",
    name: "login",
    component: () => import("pages/login.vue")
  },
  {
    path: "/welcomeback",
    name: "welcomeback",
    component: () => import("pages/welcomeback.vue")
  },
  {
    path: "/",
    component: () => import("layouts/MyLayout.vue"),

    children: [{
        path: "/menu",
        name: "menu",
        component: () => import("pages/mainmenu.vue")
      }, {
        path: "/flashcardmain",
        name: "flashcardmain",
        component: () => import("pages/flashcardmenu.vue")
      }, {
        path: "/flashcard/:level/:unit",
        name: "flashcard",
        component: () => import("pages/flashcard.vue")
      }, {
        path: "/popword/:unittype",
        name: "popword",
        component: () => import("pages/popword.vue")
      }, {
        path: "/grammarbook",
        name: "grammarbook",
        component: () => import("pages/teachinggrammar.vue")
      }, {
        path: "/grammarcontent/:unit",
        name: "grammarcontent",
        component: () => import("pages/teachinggrammarcontent.vue")
      }, {
        path: "/grammarpractice",
        name: "grammarpractice",
        component: () => import("pages/grammarpractice.vue")
      }, {
        path: "/grammartest/:startunit/:endunit/:quizno",
        name: "grammartest",
        component: () => import("pages/grammartest.vue")
      }, {
        path: "/vocabpractice",
        name: "vocabpractice",
        component: () => import("pages/vocabpractice.vue")
      }, {
        path: "/vocabtest/:startunit/:endunit/:quizno",
        name: "vocabtest",
        component: () => import("pages/vocabtest.vue")
      }, {
        path: "/popwordmenu",
        name: "popwordmenu",
        component: () => import("pages/popwordmenu.vue")
      },

    ]
  },


];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
