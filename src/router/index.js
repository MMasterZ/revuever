import Vue from "vue";
import VueRouter from "vue-router";

import routes from "./routes";
import VueLodash from "vue-lodash";

const options = {
  name: "lodash"
}; // customize the way you want to call it

Vue.use(VueLodash, options); // options is optional

Vue.use(VueRouter);

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */
var config = {
  apiKey: "AIzaSyAO6XZYR9cTvXCI-0orTpwlYm3Q2ys-FlY",
  authDomain: "winnerenglish-5f8d3.firebaseapp.com",
  databaseURL: "https://winnerenglish-5f8d3.firebaseio.com",
  projectId: "winnerenglish-5f8d3",
  storageBucket: "winnerenglish-5f8d3.appspot.com",
  messagingSenderId: "40516029824"
};

firebase.initializeApp(config);
export const db = firebase.firestore();

Vue.mixin({
  data() {
    return {
      appVersion: "1.0.10",
      currentPage: this.$q.localStorage.getItem("currentPage"),
      student: this.$q.localStorage.getItem("studentData")
    };
  },
  methods: {
    loadingShow() {
      this.$q.loading.show({
        message: "รอสักครู่...",
        delay: 400
      });
    },
    loadingHide() {
      this.$q.loading.hide();
    },
    notifyRed(messages) {
      this.$q.notify({
        color: "negative",
        position: "top",
        icon: "error",
        message: messages,
        timeout: 800
      });
    },
    notifyGreen(messages) {
      this.$q.notify({
        color: "secondary",
        position: "top",
        icon: "done",
        message: messages,
        timeout: 800
      });
    },
    async loadTime() {
      let api = "https://api.winner-english.com/data/api/gettime.php";
      let response = await axios.get(api);
      let microtime = response.data[0].microtime;

      return microtime;
    }
  },
  mounted() { }
});

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({
      x: 0,
      y: 0
    }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  });

  return Router;
}
